
# Library : -----------------------------------------------------------------
library(tidyverse)

# Data : ------------------------------------------------------------------

status=read.csv2("status_ENTREPRISE.csv",sep=";")

names(status) <- gsub("\\.", " ", names(status))

FC=read_delim("t_FC.csv", 
              delim = ",", escape_double = FALSE, trim_ws = TRUE)
FC=rename(.data = FC,SIREN=`Siren (dimension)`)

data= read_delim("data_brute.csv", 
                 delim = ";", escape_double = FALSE, trim_ws = TRUE)

link_1="https://www.bodacc.fr/pages/annonces-commerciales/?disjunctive.typeavis&disjunctive.familleavis&disjunctive.publicationavis&disjunctive.region_min&disjunctive.nom_dep_min&disjunctive.numerodepartement&sort=dateparution&q.registre=registre:"
link_2="#resultarea"



# functions : -------------------------------------------------------------


################################## detection d'erreur:
detection_erreur=function(x){
  if(is.na(x)) return("date manquante")
  if(x==0) return("pas d'erreur")
  if(x!=0) return("erreur de date")
}

detection_erreur_plan= function(x,y,z){
  if(!(z == "pas d'information" | z == "type de procedure collective n'existe pas dans le FC")){
    if(!is.na(x)) {
      if(x==0)  return("pas d'erreur")
      else  return("date fin erronee")}
    else{
      if (!is.na(y)){
        if(y==0)  return("duree renseigner mais date manquante")
        if(y!=0)  return("duree erronee")
      }
      else return("duree et date fin non-renseigner")
    }
  }
  else return("information manqaunte")
}

detection_erreur_situation=function(x,s,d){
  if(is.na(x)) return("date manquante")
  if(x==0) return("situation erronee")
  if(x!=0){
    A=filter(status,SIREN==s)
    if(any(A[4:ncol(status)] == as.Date(d), na.rm = TRUE))  return("information situation manquante")
    else  {
      if (x<=17&x>=-17) return("date et situation erronee")
      else  return("type de procedure collective n'existe pas dans le FC")
    }
  }
}
################################## Interval de vie:


Vie_SIREN = function(x){
  A=filter(FC,SIREN==x)
  Debut=min(A$`Date début activité`)
  Fin=max(A$`Date radiation`)
  return(c(Debut,Fin))
}


# Jointure ----------------------------------------------------------------
status$SIREN=as.character(status$SIREN)



######## J:
T_J=status[!is.na(status$`date d_ouverture d_une procédure de redressement`),]
T_J=left_join(x = T_J,y = FC,by="SIREN")
T_J$`Date de situation judiciaire-histo`=as.Date(T_J$`Date de situation judiciaire-histo`)
T_J=T_J[,c("SIREN","commercant","N_Departement","Date de situation judiciaire-histo","date d_ouverture d_une procédure de redressement","Situation judiciaire - historique")]
T_J=T_J[!duplicated(T_J),]
T_J$diff=as.Date(T_J$`Date de situation judiciaire-histo`)-as.Date(T_J$`date d_ouverture d_une procédure de redressement`)
T_J=filter(T_J,T_J$`Situation judiciaire - historique`=="J" | is.na(T_J$`Situation judiciaire - historique`))

T_J_exp=filter(status, !is.na(status$`date d_ouverture d_une procédure de redressement`) & !(SIREN %in% T_J$SIREN))
T_J_exp=left_join(x = T_J_exp,y = FC,by="SIREN")
if(nrow(T_J_exp)!=0){
  T_J_exp=T_J_exp[,c("SIREN","commercant","N_Departement","Date de situation judiciaire-histo","date d_ouverture d_une procédure de redressement","Situation judiciaire - historique")]
  T_J_exp$diff=as.Date(T_J_exp$`Date de situation judiciaire-histo`)-as.Date(T_J_exp$`date d_ouverture d_une procédure de redressement`)
  T_J_exp=T_J_exp[!duplicated(T_J_exp),]
  T_J_exp <- T_J_exp %>%
    group_by(SIREN) %>%
    filter(`Date de situation judiciaire-histo` == max(`Date de situation judiciaire-histo`)) %>%
    ungroup()
  T_J_exp$"Erreur"=mapply(detection_erreur_situation,T_J_exp$diff,T_J_exp$SIREN,T_J_exp$`Date de situation judiciaire-histo`)}



T_J_na=filter(T_J,is.na(T_J$`Date de situation judiciaire-histo`))
if(nrow(T_J_na)!=0){
  T_J=T_J[!is.na(T_J$`Date de situation judiciaire-histo`),]
  T_J_na=T_J_na[!duplicated(T_J_na),]
  T_J_na$"Erreur"=sapply(T_J_na$diff,detection_erreur)}


T_J <- T_J %>%
  group_by(SIREN) %>%
  filter(`Date de situation judiciaire-histo` == max(`Date de situation judiciaire-histo`)) %>%
  ungroup()
T_J$"Erreur"=sapply(T_J$diff,detection_erreur)


T_J=rbind(T_J,T_J_na,T_J_exp)
T_J$'date radiation'=lapply(X = T_J$SIREN,FUN = Vie_SIREN)
T_J$`date radiation`=sapply(T_J$`date radiation`, "[", 2)
T_J$`date radiation`=as.Date(T_J$`date radiation`)
T_J=filter(T_J,(`date d_ouverture d_une procédure de redressement`<=`date radiation` ) | (is.na(`date radiation`)))
T_J=T_J[,c("SIREN","commercant","N_Departement","date radiation","Date de situation judiciaire-histo","date d_ouverture d_une procédure de redressement","Situation judiciaire - historique","Erreur")]
colnames(T_J)=c("SIREN", "Raison sociale", "N_Departement", "Date radiation_FC", "Date de situation judiciaire histo_FC", "Date d_ouverture d_une procedure de redressement", "Situation judiciaire historique_FC", "Erreur")
T_J$URL_Bodacc=paste0(link_1,T_J$SIREN,link_2)


######## L:
T_L=status[!is.na(status$`date ouverture de liquidation judiciaire`),]
T_L=left_join(x = T_L,y = FC,by="SIREN")
T_L$`Date de situation judiciaire-histo`=as.Date(T_L$`Date de situation judiciaire-histo`)
T_L=T_L[,c("SIREN","commercant","N_Departement","Date de situation judiciaire-histo","date ouverture de liquidation judiciaire","Situation judiciaire - historique")]
T_L=T_L[!duplicated(T_L),]
T_L$diff=as.Date(T_L$`Date de situation judiciaire-histo`)-as.Date(T_L$`date ouverture de liquidation judiciaire`)
T_L=filter(T_L,T_L$`Situation judiciaire - historique`=="L" | is.na(T_L$`Situation judiciaire - historique`))

T_L_exp=filter(status, !is.na(status$`date ouverture de liquidation judiciaire`) & !(SIREN %in% T_L$SIREN))
T_L_exp=left_join(x = T_L_exp,y = FC,by="SIREN")
if(nrow(T_L_exp)!=0){
  T_L_exp=T_L_exp[,c("SIREN","commercant","N_Departement","Date de situation judiciaire-histo","date ouverture de liquidation judiciaire","Situation judiciaire - historique")]
  T_L_exp$diff=as.Date(T_L_exp$`Date de situation judiciaire-histo`)-as.Date(T_L_exp$`date ouverture de liquidation judiciaire`)
  T_L_exp=T_L_exp[!duplicated(T_L_exp),]
  T_L_exp <- T_L_exp %>%
    group_by(SIREN) %>%
    filter(`Date de situation judiciaire-histo` == max(`Date de situation judiciaire-histo`)) %>%
    ungroup()
  T_L_exp$"Erreur"=mapply(detection_erreur_situation,T_L_exp$diff,T_L_exp$SIREN,T_L_exp$`Date de situation judiciaire-histo`)}



T_L_na=filter(T_L,is.na(T_L$`Date de situation judiciaire-histo`))
if(nrow(T_L_na)!=0){
  T_L=T_L[!is.na(T_L$`Date de situation judiciaire-histo`),]
  T_L_na=T_L_na[!duplicated(T_L_na),]
  T_L_na$"Erreur"=sapply(T_L_na$diff,detection_erreur)}


T_L <- T_L %>%
  group_by(SIREN) %>%
  filter(`Date de situation judiciaire-histo` == max(`Date de situation judiciaire-histo`)) %>%
  ungroup()
T_L$"Erreur"=sapply(T_L$diff,detection_erreur)


T_L=rbind(T_L,T_L_na,T_L_exp)
T_L$'date radiation'=lapply(X = T_L$SIREN,FUN = Vie_SIREN)
T_L$`date radiation`=sapply(T_L$`date radiation`, "[", 2)
T_L$`date radiation`=as.Date(T_L$`date radiation`)
T_L=filter(T_L,(`date ouverture de liquidation judiciaire`<=`date radiation`) | (is.na(`date radiation`)))
T_L=T_L[,c("SIREN","commercant","N_Departement","date radiation","Date de situation judiciaire-histo","date ouverture de liquidation judiciaire","Situation judiciaire - historique","Erreur")]
colnames(T_L)=c("SIREN", "Raison sociale", "N_Departement", "Date radiation_FC", "Date de situation judiciaire histo_FC", "Date ouverture de liquidation judiciaire", "Situation judiciaire historique_FC", "Erreur")

T_L$URL_Bodacc=paste0(link_1,T_L$SIREN,link_2)


######## PR:
T_PR=status[!is.na(status$`date de plan de redressement`),]
T_PR=left_join(x = T_PR,y = FC,by="SIREN")
T_PR$`Date de situation judiciaire-histo`=as.Date(T_PR$`Date de situation judiciaire-histo`)
T_PR=T_PR[,c("SIREN","commercant","N_Departement","Date de situation judiciaire-histo","date de plan de redressement","Durée plan de redressement","Date fin plan de redressement","date prevue fin de redressement","Situation judiciaire - historique")]
T_PR$diff=as.Date(T_PR$`Date de situation judiciaire-histo`)-as.Date(T_PR$`date de plan de redressement`)
T_PR=filter(T_PR,T_PR$`Situation judiciaire - historique`=="PR" | is.na(T_PR$`Situation judiciaire - historique`))
T_PR=T_PR[!duplicated(T_PR),]

T_PR_exp=filter(status, !is.na(status$`date de plan de redressement`) & !(SIREN %in% T_PR$SIREN))
T_PR_exp=left_join(x = T_PR_exp,y = FC,by="SIREN")
if(nrow(T_PR_exp)!=0){
  T_PR_exp=T_PR_exp[,c("SIREN","commercant","N_Departement","Date de situation judiciaire-histo","date de plan de redressement","Durée plan de redressement","Date fin plan de redressement","date prevue fin de redressement","Situation judiciaire - historique")]
  T_PR_exp$diff=as.Date(T_PR_exp$`Date de situation judiciaire-histo`)-as.Date(T_PR_exp$`date de plan de redressement`)
  T_PR_exp=T_PR_exp[!duplicated(T_PR_exp),]
  T_PR_exp <- T_PR_exp %>%
    group_by(SIREN) %>%
    filter(`Date de situation judiciaire-histo` == max(`Date de situation judiciaire-histo`)) %>%
    ungroup()
  T_PR_exp$"Erreur date debut"=mapply(detection_erreur_situation,T_PR_exp$diff,T_PR_exp$SIREN,T_PR_exp$`Date de situation judiciaire-histo`)}

T_PR_na=filter(T_PR,is.na(T_PR$`Date de situation judiciaire-histo`))
if(nrow(T_PR_na)!=0){
  T_PR=T_PR[!is.na(T_PR$`Date de situation judiciaire-histo`),]
  T_PR_na=T_PR_na[!duplicated(T_PR_na),]
  T_PR_na$"Erreur date debut"=sapply(T_PR_na$diff,detection_erreur)}

T_PR$"Erreur date debut"=sapply(T_PR$diff,detection_erreur)
T_PR <- T_PR[!is.na(T_PR$`Date de situation judiciaire-histo`),] %>%
  group_by(SIREN) %>%
  filter(`Date de situation judiciaire-histo` == max(`Date de situation judiciaire-histo`)) %>%
  ungroup()

T_PR=rbind(T_PR,T_PR_na,T_PR_exp)
T_PR$'date radiation'=lapply(X = T_PR$SIREN,FUN = Vie_SIREN)
T_PR$`date radiation`=sapply(T_PR$`date radiation`, "[", 2)
T_PR$`date radiation`=as.Date(T_PR$`date radiation`)
T_PR=filter(T_PR,(`date de plan de redressement`<=`date radiation`)| (is.na(`date radiation`)))
T_PR$"durre"=year(T_PR$`date prevue fin de redressement`) - year(T_PR$`date de plan de redressement`)
T_PR$"diff_duree"=T_PR$`Durée plan de redressement`-T_PR$durre
T_PR$"diff_anne_fin"=as.Date(T_PR$`Date fin plan de redressement`)-as.Date(T_PR$`date prevue fin de redressement`)
T_PR$"Erreur durée de redressement"=mapply(detection_erreur_plan,T_PR$diff_anne_fin,T_PR$diff_duree,T_PR$`Erreur date debut`)
T_PR=T_PR[,c("SIREN","commercant","N_Departement","date radiation","Date de situation judiciaire-histo",
             "date de plan de redressement","Erreur date debut","Situation judiciaire - historique","Durée plan de redressement","durre","Date fin plan de redressement",
             "date prevue fin de redressement","Erreur durée de redressement")]
colnames(T_PR)=c("SIREN","Raison sociale","N_Departement","Date radiation_FC","Date de situation judiciaire-histo_FC",
                 "date de plan de redressement","Erreur date debut plan de redressement","Situation judiciaire - historique_FC","Durée plan de redressement_FC","Durée Bodacc","Date fin plan de redressement_FC",
                 "date prevue fin de redressement","Erreur durée de redressement")

T_PR$URL_Bodacc=paste0(link_1,T_PR$SIREN,link_2)




############ PS:

T_S=status[!is.na(status$`date d_ouverture d_une procédure de sauvegarde`),]
T_S=left_join(x = T_S,y = FC,by="SIREN")
T_S$`Date de situation judiciaire-histo`=as.Date(T_S$`Date de situation judiciaire-histo`)
T_S=T_S[,c("SIREN","commercant","N_Departement","Date de situation judiciaire-histo","date d_ouverture d_une procédure de sauvegarde","Situation judiciaire - historique")]
T_S=T_S[!duplicated(T_S),]
T_S$diff=as.Date(T_S$`Date de situation judiciaire-histo`)-as.Date(T_S$`date d_ouverture d_une procédure de sauvegarde`)
T_S=filter(T_S,T_S$`Situation judiciaire - historique`=="PS" | is.na(T_S$`Situation judiciaire - historique`))

T_S_exp=filter(status, !is.na(status$`date d_ouverture d_une procédure de sauvegarde`) & !(SIREN %in% T_S$SIREN))
T_S_exp=left_join(x = T_S_exp,y = FC,by="SIREN")
if(nrow(T_S_exp)!=0){
  T_S_exp=T_S_exp[,c("SIREN","commercant","N_Departement","Date de situation judiciaire-histo","date d_ouverture d_une procédure de sauvegarde","Situation judiciaire - historique")]
  T_S_exp$diff=as.Date(T_S_exp$`Date de situation judiciaire-histo`)-as.Date(T_S_exp$`date d_ouverture d_une procédure de sauvegarde`)
  T_S_exp=T_S_exp[!duplicated(T_S_exp),]
  T_S_exp <- T_S_exp %>%
    group_by(SIREN) %>%
    filter(`Date de situation judiciaire-histo` == max(`Date de situation judiciaire-histo`)) %>%
    ungroup()
  T_S_exp$"Erreur"=mapply(detection_erreur_situation,T_S_exp$diff,T_S_exp$SIREN,T_S_exp$`Date de situation judiciaire-histo`)}



T_S_na=filter(T_S,is.na(T_S$`Date de situation judiciaire-histo`))
if(nrow(T_S_na)!=0){
  T_S=T_S[!is.na(T_S$`Date de situation judiciaire-histo`),]
  T_S_na=T_S_na[!duplicated(T_S_na),]
  T_S_na$"Erreur"=sapply(T_S_na$diff,detection_erreur)}


T_S <- T_S %>%
  group_by(SIREN) %>%
  filter(`Date de situation judiciaire-histo` == max(`Date de situation judiciaire-histo`)) %>%
  ungroup()
T_S$"Erreur"=sapply(T_S$diff,detection_erreur)


T_S=rbind(T_S,T_S_na,T_S_exp)
T_S$'date radiation'=lapply(X = T_S$SIREN,FUN = Vie_SIREN)
T_S$`date radiation`=sapply(T_S$`date radiation`, "[", 2)
T_S$`date radiation`=as.Date(T_S$`date radiation`)
T_S=filter(T_S,(`date d_ouverture d_une procédure de sauvegarde`<=`date radiation` ) | (is.na(`date radiation`)))
T_S=T_S[,c("SIREN","commercant","N_Departement","date radiation","Date de situation judiciaire-histo","date d_ouverture d_une procédure de sauvegarde","Situation judiciaire - historique","Erreur")]
colnames(T_S)=c("SIREN", "Raison sociale", "N_Departement", "Date radiation_FC", "Date de situation judiciaire histo_FC", "Date d_ouverture d_une procedure de sauvegarde", "Situation judiciaire historique_FC", "Erreur")
T_S$URL_Bodacc=paste0(link_1,T_S$SIREN,link_2)








write.csv2(T_PR,"PR_DIFF.csv",row.names = FALSE)
write.csv2(T_L,"L_DIFF.csv",row.names = FALSE)
write.csv2(T_J,"J_DIFF.csv",row.names = FALSE)
write.csv2(T_S,"PS_DIFF.csv",row.names = FALSE)
