library(tidyverse)
library(jsonlite)

# donnee ------------------------------------------------------------------

data=read.csv2("data_brute.csv",sep=";")

status=read.csv2("status_ENTREPRISE.csv",sep=";")

names(status) <- gsub("\\.", " ", names(status))


FC_p=read_csv("t_FC.csv")
FC=FC_p
# functions : -------------------------------------------------------------

#################################### Espace les chiffres et lettres
ajouter_espace <- function(chaine) {
  chaine=gsub("(\\d) (\\D) ","\\1\\2",chaine)
  chaine=gsub("(\\D) (\\d) ","\\1\\2",chaine)
  # Remplace chaque chiffre suivi d'une lettre par le chiffre suivi d'un espace puis de la lettre
  chaine_modifiee <- gsub("(\\d)(\\D)", "\\1 \\2", chaine)
  # Remplace chaque lettre suivi d'un chiffre par la lettre suivi d'un espace puis du chiffre
  chaine_modifiee <- gsub("(\\D)(\\d)", "\\1 \\2", chaine_modifiee)
  # Retourne la chaîne originale si aucune substitution n'a été effectuée
  if (identical(chaine, chaine_modifiee)) {
    return(chaine)
  } else {
    return(chaine_modifiee)
  }
}
############################### Fonction transforme les mois ecrit en francais vers leur forme numerique :

mois_fr_vers_num <- function(chaine) {
  mois_map <- c("janvier|janvi" = "1", "février|fevri|févri" = "2", "mars" = "3", "avril" = "4", "mai" = "5", "juin" = "6",
                "juillet" = "7", "août" = "8", "septembre" = "9", "octobre" = "10", "novembre" = "11", "décembre" = "12")
  for (mot in names(mois_map)) {
    chaine <- gsub(paste0("\\b", mot, "\\b"), mois_map[mot], chaine, ignore.case = TRUE)
    chaine <- gsub(" ","-",chaine)
  }
  return(chaine)
}

######################### fonction d'ajustement du format data:
Inverse_date <- function(x) {
  if (as.integer(str_extract(x, "^[^-]+"))<=31) {
    parts <- strsplit(x, "-")[[1]]
    InvD <- paste(parts[3], parts[2], parts[1], sep = "-|/")
    return(InvD)
  }
  else return(x)
}
#################################### fonction de changement de numero francais  vers numerique;

remplacer_nombres_francais <- function(chaine) {
  correspondances <- c("un" = "1", "deux" = "2", "trois" = "3", "quatre" = "4", 
                       "cinq" = "5", "six" = "6", "sept" = "7", "huit" = "8", "neuf" = "9", 
                       "dix" = "10", "onze" = "11", "douze" = "12", "treize" = "13", 
                       "quatorze" = "14", "quinze" = "15", "seize" = "16", 
                       "dix-sept" = "17", "dix-huit" = "18", "dix-neuf" = "19", "vingt" = "20")
  
  for (mot in names(correspondances)) {
    chaine <- gsub(paste0("\\b", mot, "\\b"), correspondances[mot], chaine, ignore.case = TRUE)
  }
  
  return(chaine)
}
################################## detection d'erreur:
detection_erreur=function(x){
  if(is.na(x)) return("date manquante")
  if(x==0) return("pas d'erreur")
  if(x!=0) return("erreur de date")
}
################################## Interval de vie:
Inter_vie_debut=function(x){
  return(min(FC_p$`Date début activité`[FC_p$SIREN==x]))
}
Inter_vie_debut_fin=function(x){
  return(max(FC_p$`Date radiation`[FC_p$SIREN==x]))
}
########## ANNES  Bissextlie:
Bi_date=function(D){
  if (month(D)==2 && day(D)==29)   return(make_date(year(D),3,1))
  else  return(D)
}

# CODE --------------------------------------------------------------------

D=max(data$dateparution)
range=as.Date(as.Date(D):Sys.Date())
i=1 ; link=NULL
new_data=data.frame()
for (jour in range) {
  print(as.Date(jour))
  link[i] <- paste0("https://bodacc-datadila.opendatasoft.com/api/records/1.0/search/?dataset=annonces-commerciales&q=&rows=10000&facet=publicationavis_facette&facet=familleavis&facet=dateparution&refine.publicationavis_facette=Bodacc%20A&refine.familleavis=collective&refine.dateparution=", as.Date(jour))
  page <- fromJSON(url(link[i]))
  page=page[[3]]
  page=page$fields
  if (!is.null(page)) {
  page$SIREN=NA
  page$SIREN=as.integer(substr(gsub(" ", "", page$registre), start = 1, stop = 9))
  p0=page[page$SIREN%in%FC$`Siren (dimension)`,]
  p1=page[page$numerodepartement==56,]
  p0=p0[,c("id","registre","dateparution","numerodepartement","commercant","jugement","numeroannonce")]
  p1=p1[,c("id","registre","dateparution","numerodepartement","commercant","jugement","numeroannonce")]
  }
  new_data=rbind(new_data,p1,p0)
  i <- i + 1}
new_data=new_data[!duplicated(new_data),]



####################### Preparation des donnee : 
new_data <- new_data %>%
  mutate(jugement = gsub('"{4}', '"', jugement)) %>%  # Correction du format de la colonne jugement
  mutate(jugement = str_replace_all(jugement, '""', '"')) %>%  # Correction des guillemets doubles
  mutate(jugement = str_replace_all(jugement, '""""', "'")) %>%  # Correction des guillemets quadruples
  mutate(jugement = gsub("}'", "", jugement))
new_data$jugement=gsub("\\\"\\\"", "\\\"",new_data$jugement)

new_data$date= tolower(str_extract(new_data$jugement, '(?<=\"date\": \\\").*?(?=\\\")'))
new_data$complementJugement=tolower(str_extract(new_data$jugement, '(?<=\"complementJugement\": \\\").*?(?=\\\")'))
new_data$type=tolower(str_extract(new_data$jugement, '(?<=\"type\": \\\").*?(?=\\\")'))
new_data$famille=tolower(str_extract(new_data$jugement, '(?<=\"famille\": \\\").*?(?=\\\")'))
new_data$nature=tolower(str_extract(new_data$jugement, '(?<=\"nature\": \\\").*?(?=\\\")'))


################# Normalisation date :
new_data$date=mois_fr_vers_num(new_data$date)
new_data$date=gsub("er","",new_data$date)
new_data$date=gsub("[^0-9]+", "-", new_data$date)

for (i in 1:nrow(new_data)) {
  if (!is.na(new_data$date[i])) {
    new_data$date[i]=Inverse_date(new_data$date[i])
    print(i)
  }
}
new_data$date=as.Date(new_data$date)

#################################### SIREN:

new_data$SIREN=as.integer(substr(gsub(" ", "", new_data$registre), start = 1, stop = 9))


No_S=filter(new_data,is.na(new_data$SIREN))
No_S$commercant=tolower(No_S$commercant)

if (nrow(No_S)!=0) {
  data$commercant=tolower(data$commercant)
  sub_set=filter(data,!is.na(SIREN))
  d=data.frame()
  for (i in 1:nrow(No_S)) {
    print(i)
    subset_data <- filter(sub_set, commercant == No_S$commercant[i])
    d <- rbind(d, subset_data)
  }
  to_remove=c()
  for (i in unique(d$commercant)) {
    test_s <- filter(d, commercant == i)
    if (length(unique(test_s$SIREN)) != 1) {
      to_remove <- c(to_remove, i)  # Ajouter le nom de commerçant à la liste des noms à supprimer
    }
  }
  d <- d[!(d$commercant %in% to_remove), ]
  d <- d %>%
    group_by(SIREN) %>%
    slice(1)
  
  for (i in unique(d$commercant)) {
    subset_red <- data[data$commercant %in% i, ]
    data$SIREN[data$commercant %in% i] <- d$SIREN[d$commercant == i]
  }
  # Reherche dans le fichier consulaire:
  if (nrow(No_S)!=0){
    FC$`Raison sociale`=tolower(FC$`Raison sociale`)
    FC$`Raison sociale`=gsub("mr |mme ","",FC$`Raison sociale`)
    No_S=filter(new_data,is.na(new_data$SIREN))
    FC$`Raison sociale`=tolower(FC$`Raison sociale`)
    sub_set=filter(FC,!is.na(FC$`Siren (dimension)`))
    d=data.frame()
    for (i in 1:nrow(No_S)) {
      subset_data <- filter(sub_set, FC$`Raison sociale` == No_S$commercant[i])
      d <- rbind(d, subset_data)
      print(i)
    }
    d=d%>%
      filter(!is.na(d$`Siren (dimension)`))%>%
      group_by(`Raison sociale`)%>%
      distinct()
    
    to_remove=c()
    for (i in unique(d$`Raison sociale`)) {
      test_s <- filter(d, `Raison sociale` == i)
      if (length(unique(test_s$`Siren (dimension)`)) != 1) {
        to_remove <- c(to_remove, i)  # Ajouter le nom de commerçant à la liste des noms à supprimer
      }
    }
    d <- d[!(d$`Raison sociale` %in% to_remove), ]
    d <- d %>%
      group_by(`Siren (dimension)`) %>%
      slice(1)
    
    for (i in unique(d$`Raison sociale`)) {
      subset_red <- data[data$commercant %in% i, ]
      data$SIREN[data$commercant %in% i] <- d$`Siren (dimension)`[d$`Raison sociale` == i]
    }
  }
}


################## merge :
new_data$nature=gsub("prononÃ§ant|prononã§ant","prononçant",new_data$nature)
new_data$nature=gsub("procã©dure","procédure",new_data$nature)
new_data$nature=gsub("crã©ance","créance",new_data$nature)
new_data$nature=gsub("rÃ©solution|rã©solution","résolution",new_data$nature)
new_data$nature=gsub("clã´ture","cloture",new_data$nature)
new_data$nature=gsub("dã©pã´t","depot",new_data$nature)
new_data$famille=gsub("prononÃ§ant|prononã§ant","prononçant",new_data$famille)
new_data$famille=gsub("rÃ©solution|rã©solution","résolution",new_data$famille)
new_data$famille=gsub("clã´ture","cloture",new_data$famille)
new_data$famille=gsub("dã©pã´t","depot",new_data$famille)
new_data=subset(new_data,select =- jugement)
new_data$commercant=gsub(";"," ",new_data$commercant)
new_data$complementJugement=gsub(";"," ",new_data$complementJugement)
new_data$registre=gsub(";",",",new_data$registre)
new_data$nature=gsub(";",",",new_data$nature)
data=data[,c("id","registre","SIREN","dateparution","numerodepartement","commercant","numeroannonce","date","complementJugement","type","famille","nature")]
new_data=new_data[,c("id","registre","SIREN","dateparution","numerodepartement","commercant","numeroannonce","date","complementJugement","type","famille","nature")]
data=data[data$dateparution!=D,]
data_exp=rbind(data,new_data)


write.csv2(data_exp, "data_brute.csv", row.names = FALSE)





################## partition :

lj=filter(new_data,str_detect(string = nature,tolower("jugement d'ouverture de liquidation judiciaire|jugement de reprise de la procédure de liquidation judiciaire|jugement de conversion en liquidation judiciaire|jugement prononçant la résolution du plan de sauvegarde et la liquidation judiciaire|jugement prononçant la résolution du plan de redressement et la liquidation judiciaire|jugement d'extension de liquidation judiciaire")))
lj <- lj %>% arrange(SIREN, desc(date))%>% distinct(SIREN, .keep_all = TRUE)
ljc=filter(new_data,str_detect(string = nature,tolower("arrêt de la cour d'appel infirmant une décision soumise à publicité")))
ljc <- ljc %>% arrange(SIREN, desc(date))%>% distinct(SIREN, .keep_all = TRUE)
pr=filter(new_data,str_detect(nature,tolower('Jugement de plan de redressement')))
pr <- pr %>% arrange(SIREN, desc(date))%>% distinct(SIREN, .keep_all = TRUE)
fpr=filter(new_data,str_detect(nature,tolower('Jugement mettant fin à la procédure de redressement judiciaire')))
fpr <- fpr %>% arrange(SIREN, desc(date))%>% distinct(SIREN, .keep_all = TRUE)
xpr=filter(new_data,str_detect(nature,tolower("Jugement d'extension d'une procédure de redressement judiciaire|Jugement d'extension d'une procedure de redressement judiciaire")))
xpr <- xpr %>% arrange(SIREN, desc(date))%>% distinct(SIREN, .keep_all = TRUE)
xr_to_sauv=filter(new_data,str_detect(nature,tolower("Jugement de conversion en redressement judiciaire de la procédure de sauvegarde|Jugement de conversion en redressement judiciaire de la procedure de sauvegarde")))
xr_to_sauv <- xr_to_sauv %>% arrange(SIREN, desc(date))%>% distinct(SIREN, .keep_all = TRUE)
pro_pr=filter(new_data,str_detect(nature,tolower("Jugement prononçant la résolution du plan de redressement|Jugement prononçant la resolution du plan de redressement")))
pro_pr <- pro_pr %>% arrange(SIREN, desc(date))%>% distinct(SIREN, .keep_all = TRUE)
ouv_pr=filter(new_data,str_detect(nature,tolower("Jugement d'ouverture d'une procédure de redressement judiciaire|Jugement d'ouverture d'une procedure de redressement judiciaire")))
ouv_pr <- ouv_pr %>% arrange(SIREN, desc(date))%>% distinct(SIREN, .keep_all = TRUE)
pc=filter(new_data,str_detect(nature,tolower("Jugement modifiant le plan de continuation")))
pc <- pc %>% arrange(SIREN, desc(date))%>% distinct(SIREN, .keep_all = TRUE)
mod_p=filter(new_data,str_detect(nature,tolower("jugement modifiant le plan de redressement")))
mod_p <- mod_p %>% arrange(SIREN, desc(date))%>% distinct(SIREN, .keep_all = TRUE)
ouv_ps=filter(new_data,str_detect(nature,tolower("jugement d'ouverture d'une procédure de sauvegarde|jugement d'ouverture d'une procédure de sauvegarde")))
ouv_ps <- ouv_ps %>% arrange(SIREN, desc(date))%>% distinct(SIREN, .keep_all = TRUE)
ps=filter(new_data,str_detect(nature,tolower("jugement arrêtant le plan de sauvegarde|jugement arretant le plan de sauvegarde")))
ps <- ps %>% arrange(SIREN, desc(date))%>% distinct(SIREN, .keep_all = TRUE)
mod_ps=filter(new_data,str_detect(nature,tolower("jugement modifiant le plan de sauvegarde")))
mod_ps <- mod_ps %>% arrange(SIREN, desc(date))%>% distinct(SIREN, .keep_all = TRUE)


################## extraction duree de redressement :
if (nrow(pr)!=0){
  pr$complementJugement=ajouter_espace(pr$complementJugement)
  cm <- character(length(pr$complementJugement))  # Initialisation du vecteur cm
  for (i in 1:length(pr$complementJugement)) {
    chaine_arretee=str_extract(pr$complementJugement[i], "(.{10})\\s(?:ans|annees|annee|années|année|an|nomme)")
    cm[i] <- remplacer_nombres_francais(chaine_arretee)
  }
  annes=NULL
  for (i in 1:length(cm)) {
    chaine_arretee=str_extract(cm[i], "(.{3})\\s(?:ans|annees|annee|années|année|an|nomme)")
    annes[i]=str_extract(chaine_arretee, "\\d+")
  }
  duree_annes=as.integer(annes)
  pr=data.frame(pr,duree_annes)
  ######## Extraction mois
  
  for (i in 1:length(pr$complementJugement)) {
    chaine_arretee=str_extract(pr$complementJugement[i], "(.{10})\\s(?:mois)")
    cm[i] <- remplacer_nombres_francais(chaine_arretee)
  }
  mois=NULL
  for (i in 1:length(cm)) {
    chaine_arretee=str_extract(cm[i], "(.{4})\\s(?:mois)")
    mois[i]=str_extract(chaine_arretee, "\\d+")
  }
  duree_mois=as.integer(mois)
  duree_mois[is.na(mois)]=0
  pr=data.frame(pr,duree_mois)
  ################ calcule date fin de redressment :
  annes=pr$duree_annes
  annes[is.na(annes)]=0
  DD=as.Date(sapply(pr$date,Bi_date))
  date_fin=DD+lubridate::years(annes) + months(pr$duree_mois)
  pr=data.frame(pr,date_fin)
}


################ extraction duree de sauvegarde :
if (nrow(ps)!=0) {
  ps$complementJugement=ajouter_espace(ps$complementJugement)
  ps=ps[!str_detect(ps$complementJugement,"fin du plan|accélérée"),]
  cm <- character(length(ps$complementJugement))  # Initialisation du vecteur cm
  for (i in 1:length(ps$complementJugement)) {
    chaine_arretee=str_extract(ps$complementJugement[i], "(.{10})\\s(?:an|nommant)")
    cm[i] <- remplacer_nombres_francais(chaine_arretee)
  }
  
  annes=NULL
  for (i in 1:length(cm)) {
    chaine_arretee=str_extract(cm[i], "(.{3})\\s(?:an|nommant)")
    annes[i]=str_extract(chaine_arretee, "\\d+")
  }
  duree_annes=as.integer(annes)
  ps=data.frame(ps,duree_annes)
  ######## Extraction mois
  
  for (i in 1:length(ps$complementJugement)) {
    chaine_arretee=str_extract(ps$complementJugement[i], "(.{10})\\s(?:mois)")
    cm[i] <- remplacer_nombres_francais(chaine_arretee)
  }
  mois=NULL
  for (i in 1:length(cm)) {
    chaine_arretee=str_extract(cm[i], "(.{4})\\s(?:mois)")
    mois[i]=str_extract(chaine_arretee, "\\d+")
  }
  duree_mois=as.integer(mois)
  duree_mois[is.na(mois)]=0
  ps=data.frame(ps,duree_mois)
  ################ calcule date fin de redressment :
  annes=ps$duree_annes
  annes[is.na(annes)]=0
  DD=as.Date(sapply(ps$date,Bi_date))
  date_fin=DD+lubridate::years(annes) + months(ps$duree_mois)
  ps=data.frame(ps,date_fin)
  
}



######### Status entreprise :




status$`date de plan de redressement`=as.Date(status$`date de plan de redressement`)
status$`date de modification de plan de redressement`=as.Date(status$`date de modification de plan de redressement`)
status$`date prevue fin de redressement`=as.Date(status$`date prevue fin de redressement`)
status$`date ouverture de liquidation judiciaire`=as.Date(status$`date ouverture de liquidation judiciaire`)
status$`date plan de continuation`=as.Date(status$`date plan de continuation`)
status$`date d_ouverture d_une procédure de redressement`=as.Date(status$`date d_ouverture d_une procédure de redressement`)
status$`date prononçant la résolution du plan de redressement et la liquidation judiciaire`=as.Date(status$`date prononçant la résolution du plan de redressement et la liquidation judiciaire`)
status$`date extension d_une procédure de redressement judiciaire`=as.Date(status$`date extension d_une procédure de redressement judiciaire`)
status$`date conversion en redressement judiciaire de la procédure de sauvegarde`=as.Date(status$`date conversion en redressement judiciaire de la procédure de sauvegarde`)
status$`date mettant fin à la procédure de redressement judiciaire`=as.Date(status$`date mettant fin à la procédure de redressement judiciaire`)
status$`date d_ouverture d_une procédure de sauvegarde`=as.Date(status$`date d_ouverture d_une procédure de sauvegarde`)
status$`date de plan de sauvegarde`=as.Date(status$`date de plan de sauvegarde`)
status$`date de modification le plan de sauvegarde`=as.Date(status$`date de modification le plan de sauvegarde`)
status$`date prevue fin de sauvegarde`=as.Date(status$`date prevue fin de sauvegarde`)
status$`arrêt de la cour d_appel infirmant une décision soumise à publicité`=as.Date(status$`arrêt de la cour d_appel infirmant une décision soumise à publicité`)



status$N_Departement[status$SIREN %in% data$SIREN] <- data$numerodepartement[match(status$SIREN[status$SIREN %in% data$SIREN], data$SIREN)]
status$`date plan de continuation`[status$SIREN %in% pc$SIREN] <- pc$date[match(status$SIREN[status$SIREN %in% pc$SIREN], pc$SIREN)]
status$`date de plan de redressement`[status$SIREN %in% pr$SIREN] <- pr$date[match(status$SIREN[status$SIREN %in% pr$SIREN], pr$SIREN)]
status$`date prevue fin de redressement`[status$SIREN %in% pr$SIREN] <- pr$date_fin[match(status$SIREN[status$SIREN %in% pr$SIREN], pr$SIREN)]
status$`date de plan de sauvegarde`[status$SIREN %in% ps$SIREN] <- ps$date[match(status$SIREN[status$SIREN %in% ps$SIREN], ps$SIREN)]
status$`date prevue fin de sauvegarde`[status$SIREN %in% ps$SIREN] <- ps$date_fin[match(status$SIREN[status$SIREN %in% ps$SIREN], ps$SIREN)]
status$`date d_ouverture d_une procédure de sauvegarde`[status$SIREN %in% ouv_ps$SIREN] <-ouv_ps$date[match(status$SIREN[status$SIREN %in% ouv_ps$SIREN], ouv_ps$SIREN)]
status$`date de modification de plan de redressement`[status$SIREN %in% mod_p$SIREN] <- mod_p$date[match(status$SIREN[status$SIREN %in% mod_p$SIREN], mod_p$SIREN)]
status$`date ouverture de liquidation judiciaire`[status$SIREN %in% lj$SIREN] <- lj$date[match(status$SIREN[status$SIREN %in% lj$SIREN], lj$SIREN)]
status$`date mettant fin à la procédure de redressement judiciaire`[status$SIREN %in% fpr$SIREN] <- fpr$date[match(status$SIREN[status$SIREN %in% fpr$SIREN], fpr$SIREN)]
status$`date conversion en redressement judiciaire de la procédure de sauvegarde`[status$SIREN %in% xr_to_sauv$SIREN] <- xr_to_sauv$date[match(status$SIREN[status$SIREN %in% xr_to_sauv$SIREN], xr_to_sauv$SIREN)]
status$`date extension d_une procédure de redressement judiciaire`[status$SIREN %in% xpr$SIREN] <- xpr$date[match(status$SIREN[status$SIREN %in% xpr$SIREN], xpr$SIREN)]
status$`date prononçant la résolution du plan de redressement et la liquidation judiciaire`[status$SIREN %in% pro_pr$SIREN] <- pro_pr$date[match(status$SIREN[status$SIREN %in% pro_pr$SIREN], pro_pr$SIREN)]
status$`date d_ouverture d_une procédure de redressement`[status$SIREN %in% ouv_pr$SIREN] <- ouv_pr$date[match(status$SIREN[status$SIREN %in% ouv_pr$SIREN], ouv_pr$SIREN)]
status$`date de modification le plan de sauvegarde`[status$SIREN %in% mod_ps$SIREN] <- mod_ps$date[match(status$SIREN[status$SIREN %in% mod_ps$SIREN], mod_ps$SIREN)]
status$`arrêt de la cour d_appel infirmant une décision soumise à publicité`[status$SIREN %in% ljc$SIREN] <- ljc$date[match(status$SIREN[status$SIREN %in% ljc$SIREN], ljc$SIREN)]





write.csv2(status,"status_ENTREPRISE.csv",row.names = FALSE)
